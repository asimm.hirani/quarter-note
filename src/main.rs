use std::{time::Duration, iter::Map, collections::HashMap};

use cpal::{traits::{HostTrait, DeviceTrait, StreamTrait}, SampleFormat};

use std::collections::BinaryHeap;

struct SineGenerator {
    sampling_freq : u32,
    sample_number : u64
}

impl SineGenerator{
    fn new(freq : u32) -> Self {
        SineGenerator { sampling_freq: freq, sample_number: 0 }
    }
    fn write_60hz_sine(&mut self, data: &mut [f32], _: &cpal::OutputCallbackInfo) {
        const ARGUMENT: f32 = 2.0*3.1415926*60.0;
        for sample in data.iter_mut() {
            let w = ARGUMENT*(self.sample_number as f32)/(self.sampling_freq as f32);
            *sample = (w.sin()).into();
            self.sample_number = self.sample_number+1;
        }
        //println!("Writing Sample {} to the device!", self.sample_number);
    }
}


fn main() {
    
    let host = cpal::default_host();
    let device = host.default_output_device()
        .expect("No output device available!");
    let mut supported_configs_range = device.supported_output_configs()
        .expect("Error while parsing configurations!");
    let mut target_config = supported_configs_range.next().unwrap().with_max_sample_rate();
    for config in supported_configs_range {
        if config.sample_format() == SampleFormat::F32 {
            target_config = config.with_max_sample_rate();
        }
    }
    let sample_rate = target_config.sample_rate();
    // let sample_format = supported_config.sample_format();
    let config = target_config.config();
    let mut sine_generator = SineGenerator::new(sample_rate.0);

    let stream = device.build_output_stream(
        &config,
        move |data: &mut [f32], o: &cpal::OutputCallbackInfo| {
            sine_generator.write_60hz_sine(data, o);
        },
        move |err| {
            panic!("Error: {}", err.to_string());
        },
        None
    ).expect("The stream could not be configured!");
    stream.play().expect("Could not play stream!");

    std::thread::sleep(Duration::from_secs(1));
}

struct Note {
    noteType : NoteType,
    noteId : i32,
    tStart : f32,
    tEnd : f32,
    freq : f32,
    amplitude : f32
}
enum NoteType {
    Square,
    Sine,
    Triangle,
    Sawtooth
}

trait Playable {
    fn get_sample(&self, t: f32) -> f32;
}

impl Playable for Note {
    fn get_sample(&self, t: f32) -> f32 {
        0.0
    }
}

impl Note {
    fn to_events(&self) -> (NoteEvent, NoteEvent) {
        (
            NoteEvent::NoteStart(&self),
            NoteEvent::NoteEnd(&self)
        )
    }
}


#[derive(Eq, Ord, PartialEq, PartialOrd)]
enum NoteEvent<'a> {
    NoteStart(&'a Note),
    NoteEnd(&'a Note)
}

impl NoteEvent<'_> {
    fn time(&self) -> f32{
        if let NoteEvent::NoteStart(note) = *self {
            return note.tStart;
        } else if let NoteEvent::NoteEnd(note) = *self {
            return note.tEnd;
        } else {
            panic!("Unexpected NoteEvent!");
        }
    }
}

struct Composition<'a> {
    noteList : Vec<Note>, // List of notes to play
    tStart : f32, // The starting time of the composition in seconds
    tEnd : f32, // The ending time of the composition in seconds
    noteEventHeap : BinaryHeap<NoteEvent<'a>>
}

impl Composition<'_> {
    fn add_note(&mut self, to_add : Note) {
        
        self.noteList.push(to_add);
    }

    fn play(&self) {

    }
}
